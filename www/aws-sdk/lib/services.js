var fs = require(['./fs/fs']);
var path = require(['./path/path']);
var AWS = require(['./aws-sdk/lib/core']);
var apis = require(['./aws-sdk/lib/api_loader']);

// define services using map
apis.services.forEach(function(identifier) {
  var name = apis.serviceName(identifier);
  var versions = apis.serviceVersions(identifier);
  AWS[name] = AWS.Service.defineService(identifier, versions);

  // load any customizations from lib/services/<svcidentifier>.js
  var svcFile = path.join(__dirname, 'awk-sdk/lib/services', identifier + '.js');
  if (fs.existsSync(svcFile)) require('./services/' + identifier);
});
