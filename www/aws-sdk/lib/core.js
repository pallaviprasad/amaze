/**
 * The main AWS namespace
 */
var AWS = { util: require(['./aws-sdk/lib/util']) };

/**
 * @api private
 * @!macro [new] nobrowser
 *   @note This feature is not supported in the browser environment of the SDK.
 */
var _hidden = {}; _hidden.toString(); // hack to parse macro

module.exports = AWS;

AWS.util.update(AWS, {

  /**
   * @constant
   */
  VERSION: '2.1.32',

  /**
   * @api private
   */
  Signers: {},

  /**
   * @api private
   */
  Protocol: {
    Json: require(['./aws-sdk/lib/protocol/json']),
    Query: require(['./aws-sdk/lib/protocol/query']),
    Rest: require(['./aws-sdk/lib/protocol/rest']),
    RestJson: require(['./aws-sdk/lib/protocol/rest_json']),
    RestXml: require(['./aws-sdk/lib/protocol/rest_xml'])
  },

  /**
   * @api private
   */
  XML: {
    Builder: require(['./aws-sdk/lib/xml/builder']),
    Parser: null // conditionally set based on environment
  },

  /**
   * @api private
   */
  JSON: {
    Builder: require(['./aws-sdk/lib/json/builder']),
    Parser: require(['./aws-sdk/lib/json/parser'])
  },

  /**
   * @api private
   */
  Model: {
    Api: require(['./aws-sdk/lib/model/api']),
    Operation: require(['./aws-sdk/lib/model/operation']),
    Shape: require(['./aws-sdk/lib/model/shape']),
    Paginator: require(['./aws-sdk/lib/model/paginator']),
    ResourceWaiter: require(['./aws-sdk/lib/model/resource_waiter'])
  },

  util: require(['./aws-sdk/lib/util']),

  /**
   * @api private
   */
  apiLoader: function() { throw new Error('No API loader set'); }
});

require(['./aws-sdk/lib/service']);

require(['./aws-sdk/lib/credentials']);
require(['./aws-sdk/lib/credentials/credential_provider_chain']);
require(['./aws-sdk/lib/credentials/temporary_credentials']);
require(['./aws-sdk/lib/credentials/web_identity_credentials']);
require(['./aws-sdk/lib/credentials/cognito_identity_credentials']);
require(['./aws-sdk/lib/credentials/saml_credentials']);

require(['./aws-sdk/lib/config']);
require(['./aws-sdk/lib/http']);
require(['./aws-sdk/lib/sequential_executor']);
require(['./aws-sdk/lib/event_listeners']);
require(['./aws-sdk/lib/request']);
require(['./aws-sdk/lib/response']);
require(['./aws-sdk/lib/resource_waiter']);
require(['./aws-sdk/lib/signers/request_signer']);
require(['./aws-sdk/lib/param_validator']);

/**
 * @readonly
 * @return [AWS.SequentialExecutor] a collection of global event listeners that
 *   are attached to every sent request.
 * @see AWS.Request AWS.Request for a list of events to listen for
 * @example Logging the time taken to send a request
 *   AWS.events.on('send', function startSend(resp) {
 *     resp.startTime = new Date().getTime();
 *   }).on('complete', function calculateTime(resp) {
 *     var time = (new Date().getTime() - resp.startTime) / 1000;
 *     console.log('Request took ' + time + ' seconds');
 *   });
 *
 *   new AWS.S3().listBuckets(); // prints 'Request took 0.285 seconds'
 */
AWS.events = new AWS.SequentialExecutor();
