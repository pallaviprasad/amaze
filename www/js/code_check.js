function getBucketObjects(csvBucket){
    var str = "aws-billing-csv";

    if(csvBucket !== undefined){
        //console.log("Bucket(getBucketObjects) :: ", csvBucket);
        s3Client.listObjects(params = {Bucket: csvBucket}, function(err, data){
            if(err) alert('Could not load objects from ' + csvBucket);
            else{
                for(var index in data.Contents){
                    var fileKey = data.Contents[index].Key;
                    if(fileKey.search(str) != -1){
                        insertValue(fileKey, csvBucket);
                        
                        s3Client.getObject(params = {Bucket: csvBucket, Key: fileKey }, function(err,data){    
                            if(err) alert('Could not get the object');
                            else{
                                
                                CSVData.parse(data);
                                //console.log(data);
                                //storing the contents
                                // var filename = fileKey.substr(0,fileKey.lastIndexOf('.')) || fileKey;
                                // var storeKey = filename.substr(filename.length - 7);

                               // var fail = failCB('Local file System');
                                // window.requestFileSystem(LocalFileSystem.PERSISTENT,0,function(fs){
                                //     fs.root.getFile(fileKey, {create:false}, function(fileEntry){
                                //         fileEntry.file(function(file){
                                //             file.fileReader = new fileReader();
                                //             fileReader.onloadend=function(evt){
                                //                 CSVData.parse(evt.target.result);
                                //             };
                                //             fileReader.readAsText(file);
                                //         }, function(error){
                                //             alert("Got Error while reading");
                                //         });
                                //     },function(error){
                                //         alert(fileKey + "not present. Writing into System");
                                //         fs.root.getFile(fileKey, {create: true},function(fileEntry){
                                //             fileEntry.createWriter(function(fileWriter){
                                //                 fileWriter.onwrite=function(evt){
                                //                     alert(fileKey + 'written successfully');
                                //                 };
                                //                 fileWriter.write(fileKey);
                                //             });
                                //         }, function(error){
                                //             alert('Write unsuccessful');
                                //         });
                                //     });
                                // },function(error){
                                //     alert('requestFileSystem failed');
                                // });
                            }
                        });   
                    }
                }
            }
          //  window.location='dashboard.html';
        });
        //loadPage('./dashboard.html');
    }
    else{alert('csvBucket undefined');}
}

/**
 * Load page into ur
 *
 * @param url           The url to load
 */
// function loadPage(url) {
//     var xmlhttp = new XMLHttpRequest();

//     // Callback function when XMLHttpRequest is ready
//     xmlhttp.onreadystatechange=function(){
//         if (xmlhttp.readyState === 4){
//             if (xmlhttp.status === 200) {
//                 document.getElementById('container').innerHTML = xmlhttp.responseText;
//             }
//         }
//     };
//     xmlhttp.open("GET", url , true);
//     xmlhttp.send();
// }

// ref: http://stackoverflow.com/a/1293163/2343
// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray( strData, strDelimiter ){
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );


    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;


    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec( strData )){

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){

            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push( [] );

        }

        var strMatchedValue;

        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[ 2 ]){

            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {

            // We found a non-quoted value.
            strMatchedValue = arrMatches[ 3 ];

        }


        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }

    // Return the parsed data.
    return( arrData );
}

$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "data.txt",
        dataType: "text",
        success: function(data) {processData(data);}
     });
});

function processData(allText) {
    var allTextLines = allText.split(/\r\n|\n/);
    var headers = allTextLines[0].split(',');
    var lines = [];

    for (var i=1; i<allTextLines.length; i++) {
        var data = allTextLines[i].split(',');
        if (data.length == headers.length) {

            var tarr = [];
            for (var j=0; j<headers.length; j++) {
                tarr.push(headers[j]+":"+data[j]);
            }
            lines.push(tarr);
        }
    }
    // alert(lines);
}

