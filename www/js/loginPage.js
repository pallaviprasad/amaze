AWS = require(['aws-sdk-2.1.34.min.js']);

var fileKey, counter;
var flag = true;

var fileList = new Array();

function pageLoad(){
    document.addEventListener('deviceReady', onDeviceReady, false);

} 

function onDeviceReady(){
    //FastClick.attach(document.body);   //avoids the 300ms delay
    document.getElementById('help').addEventListener("click", showHelpAlert, false);
    document.getElementById('submitButton').addEventListener("click", startApp,false);
    //document.getElementById('detail').addEventListener('click', showDetailedList,false);
    // var fail = failCB('LocalFileSystem');
    // window.requestFileSystem(LocalFileSystem.PERSISTENT,0,gotFS,fail);
    

}

function showHelpAlert(){
    navigator.notification.alert(
        "AccessKey and SecretAccessKey Combination Incorrect or One of the fields is empty! Please check and Retry!",
        function(){},
        "Key Mismatch",
        "Done");
}

function startApp(){    
    $('#loading').removeClass('hide').addClass('show');

    accKey = $("#accessKey").val();
    secKey = $("#secretKey").val();
    
    //createTable();
    if (empty(accKey)) {
        navigator.notification.alert(
            "AccessKey and SecretAccessKey Combination Incorrect or One of the fields is empty! Please check and Retry!",
            function(){},
            "Key Mismatch",
            "Done");
        //alert("AccessKey and SecretAccessKey fields are empty! Please enter values and Retry!");
        $("#accessKey").focus();
    } else {
        if (empty(secKey)) {
            navigator.notification.alert(
                "AccessKey and SecretAccessKey Combination Incorrect or One of the fields is empty! Please check and Retry!",
                function(){},
                "Key Mismatch",
                "Done");
            // alert("AccessKey and SecretAccessKey fields are empty! Please enter values and Retry!");
            $("#accessKey").focus();
        } else {
            bucketListing(accKey, secKey);
        }
    }
}

function bucketListing(accKey, secKey){
    var bucketListObj = {};
    var bucketListArr = [];

    AWS.config.update({
        accessKeyId: accKey,
        secretAccessKey: secKey
    });
    //'AKIAIOGWSW2QRO5CXEGQ', secretAccessKey:'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'});

    s3Client = new AWS.S3();

    var bucketCount;
    s3Client.listBuckets(function(err, data) {
        if (err){ 
            alert("Error :: " + err);
            //window.location = 'configPage.html';
        } 
        else {
            if ($.isEmptyObject(data)){
                window.location = 'configPage.html';
            }
            else{                
                for (var index in data.Buckets) {
                    var bucket = data.Buckets[index];
                    bucketListArr.push({
                        "bucketName": bucket.Name,
                        "creationDate": bucket.CreationDate
                    });
                }
            }
            //console.log('Bucket List :: ', bucketListArr);
            sortBuckets(bucketListArr, "creationDate");    
        }
    });
    //window.location= 'dashboard.html';
}      

function sortBuckets(bucketListArr, strParamToSortOn){
    //console.log('bucket list :: ', bucketListArr);
    bucketListArr.sort(function (a, b) {
        var dateA = a.creationDate, dateB = b.creationDate;
        return dateB - dateA;    //sorts in descending order
    });

    getCSVFiles(bucketListArr);
}

function getCSVFiles(bucketListArr){
    var counter, j = 0, csvBucket;
    var str = "aws-billing-csv";
    var flag = 0;
    //console.log("Bucket List :: ", bucketListArr);

     bucketListArr.some(function(bucket, index) {
            (function(bucketId) {
                //  var bucketObj = {};
                // console.log("Bucket Id :: ",bucketId);
                s3Client.listObjects(params = {Bucket: bucketId }, function(err, data) {
                    var csvBucketArr = [];
                    if (err) { alert('Could not load objects from ' + bucketId);
                       // document.getElementById('status').innerHTML = 'Could not load objects from ' + bucketId;
                    }
                    else{
                       
                        //console.log("Bucket Data "+index+" :: ",data);
                        data.Contents.some(function(content,contentIndex){
                            var bucketObj = {};
                            var fileKey = content.Key;
                            if(fileKey.search(str) != -1) {
                                csvBucket = bucketId;
                                 return true;
                                //console.log("bucket :: ", csvBucket);
                                //console.log("fileKey["+bucketId+"] "+contentIndex+" :: ",fileKey + "  Date :: " ,content.LastModified);
                            //     bucketObj[fileKey] = {
                            //         lastModified    : content.LastModified,
                            //         bucketName      : bucketId,
                            //         fileKey         : fileKey
                            //     }
                            //     if(!jQuery.isEmptyObject(bucketObj)){
                            //         csvBucketArr.push(bucketObj);
                            //     }
                             }
                        });
                        //csv = csvBucketArr;
                    }
                    if(csvBucket === undefined)
                        flag++;
                    
                    if (flag === bucketListArr.length)
                        window.open('configPage.html','_self');
                    if(csvBucket !== undefined){
                        //console.log("Bucket :: ", csvBucket);
                        getBucketObjects(csvBucket);
                    }

                    //return true;
                        // if(csvBucketArr.length!==0)
                        //     console.log("csvBucketArr :: ",csvBucketArr);
                    
                });
                
            }(bucket.bucketName));
            // console.log("Bucket " + index + " :: ", bucket);
           
        });
    //window.location = 'dashboard.html';
}

function bucketCreate(){

    var newBucketName = 'Billing-TC-' + Number(new Date());

    $('#configPanel').replaceWith('<div class="amazeConfig" align="center"><img src="img/cartoon.png"/><p><b>Kindly bear with me</b></p><p>while I create a bucket for you.</p><p>It will take a few seconds</p></div>');
    s3Client.createBucket(params={Bucket: newBucketName, ACL:'public-read-write'}, function(err,data){
        if(err){alert('createBucket' + err);}
        else{
            console.log(data);
            $('#configPanel').replaceWith('<div class="amazeConfig" align="center"><p><b>DONE!!!</b></p><p>Your S3 bucket is created.</p></div>'+
                    '<div><p>Kindly copy or memorize the name</p><p> of the bucket. We will need it going forward.</p></div>' + 
                    '<div><label>Bucket Name:</label><br>' + 
                    '<input type="text" value="'+ newBucketName +'"id="bucketValue"/><div id="copyButton"><p>' + 
                    '<a href="URL.htm">Copy Bucket Name</a></p></div></div>'+
                    '<div><button onclick="nextStep()">NEXT</button></div>');
             
        }

    });
}


function nextStep(){
    $('#configPanel').replaceWith('<div id="screenShot"><img src="img/screen-shot-example (1).png"/></div>' +
        '<div class="footer-container" onclick="nextPage()">'+
            '<h3>Detailed Billing</h3></div>');
}

function nextPage(){

}

function showDetailedList(){
    $('#container').removeClass('show').addClass('hide');
    // $('#content-Container').removeClass('show').addClass('hide');
    // $('#canvas-holder').removeClass('show').addClass('hide');
    // $('#fileList').removeClass('show').addClass('hide');
    $('#servicesDetails').removeClass('hide').addClass('show');
    $('#servicesList').removeClass('hide').addClass('show');
    //$('#detail').html('<h3 onclick="prevPage()">Back</h3>');

    //$('#head').html("<img src='img/eye.png'>"+
      //          "<h3 style='display: initial'>Detailed Billing</h3>");

}

function prevPage(){
    $('#servicesDetails').removeClass('show').addClass('hide');
    //$('#servicesList').removeClass('show').addClass('hide');
    // $('#content-Container').removeClass('hide').addClass('show');
    // $('#canvas-holder').removeClass('hide').addClass('show');
    // $('#fileList').removeClass('hide').addClass('show');
   // $('#detail').html(htmlString);
    $('#container').removeClass('hide').addClass('show');
}

function slideMenu(){
    var hidden = $('.slider-menu');
    if (hidden.hasClass('show')){
        hidden.animate({"left":"-1000px"}, "slow").removeClass('show').addClass('hide');
    } else {
        hidden.animate({"left":"0px"}, "slow").removeClass('hide').addClass('show');;
    }
}

$(document.body).click(function(){
    var hidden = $('.slider-menu');
    if (hidden.hasClass('show')){
        hidden.animate({"left":"-1000px"}, "slow").removeClass('show').addClass('hide');
    } else {
        hidden.animate({"left":"0px"}, "slow").removeClass('hide').addClass('show');;
    }
});