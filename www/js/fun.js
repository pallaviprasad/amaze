function empty(data)
{
  if(typeof(data) == 'number' || typeof(data) == 'boolean')
  { 
    return false; 
  }
  if(typeof(data) == 'undefined' || data === null)
  {
    return true; 
  }
  if(typeof(data.length) != 'undefined')
  {
    return data.length == 0;
  }
  var count = 0;
  for(var i in data)
  {
    if(data.hasOwnProperty(i))
    {
      count ++;
    }
  }
  return count == 0;
}

function onRequestError(err){
        console.log("Request File System failure", err);
}

function onError(e){
  console.log("ERROR");
  console.log(JSON.stringify(e));
}

function errorFn(error, file)
{
  console.log("ERROR:", error, file);
}

function completeFn(){
  console.log(arguments);
}

function getBucketObjects(csvBucket){
    var str = "aws-billing-csv";

    if(csvBucket !== undefined){
        //console.log("Bucket(getBucketObjects) :: ", csvBucket);
        s3Client.listObjects(params = {Bucket: csvBucket}, function(err, data){
            if(err) alert('Could not load objects from ' + csvBucket);
            else{
                fileKeys=[];
                for(var index in data.Contents){
                    var fileKey = data.Contents[index].Key;
                    if(fileKey.search(str) != -1){
                      var FileObj={'bucketName': csvBucket, 'fileName': fileKey};
                      fileKeys.push(FileObj);
                    }
                } //for loop
                for(var i in fileKeys){
                  storeFile(i);
                }
              }
        });
    }
    else{alert('csvBucket undefined');}
}


function storeFile(index){
  //console.log('file key',fileKeys);
  var arrLength = fileKeys.length;
  //console.log("Length of Array :: ", arrLength);
  var dataObj = fileKeys[index];
  //console.log('dataObject',dataObj);
  
  s3Client.getObject(params = {Bucket: dataObj.bucketName, Key: dataObj.fileName, ResponseContentType: "text/csv"},
    function(err,data){
      if(err) console.log("Error :: ", err.stack);
      else{
        //console.log(data.Body.toString());
     // }
        window.requestFileSystem(LocalFileSystem.PERSISTENT,0, 
          function(fs){
            //alert(fs.root.fullPath); 
            fs.root.getDirectory("Amazedata", {create: true}, 
              function(d){
                d.getFile(dataObj.fileName, {create: true, exclusive: false}, 
                  function(fileEntry){
                    fileEntry.createWriter(function(fileWriter){
                        fileWriter.onwrite= function(evt){
                          //alert("Write was successful" + dataObj.fileName);
                        };
                        fileWriter.write(data.Body.toString());
                       // console.log("index :: ", index, "  arrLength -1 :: ", arrLength-1);
                        if (index == (arrLength-1)){
                         // alert('switching windows.');
                           //window.location='dashboard.html';
                           $('#loading').removeClass('show').addClass('hide');
                           $('#start').addClass('hide');
                           $('#container').removeClass('hide').addClass('show');
                           $('#loading1').removeClass('hide').addClass('show');
                           getCSV();
                        }
                        //     storeFile(index++);
                        // }
                        // else{
                        // }

                      },function(error){
                        alert("Failed to get the File Writer");
                      });
                  
                  }, onError);
            }, onError);
        }, onRequestError);
      }
   });    
}


function getCSV(){
  var knownFiles=[];

  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
    function(fs){
      fs.root.getDirectory('Amazedata', null, 
        function(d){
          var dirReader = d.createReader();
          dirReader.readEntries(function(entries){
            if (!entries.length) {
              alert('FileSystem is Empty');
            } //end if
            else{
              var dirLength = entries.length;
              payerAccArr = [];
              sixMonths = getSixMonthsData();
            // console.log(sixMonths);
              for(var index =0; index<dirLength;index++){
                for(var x=0; x<sixMonths.length;x++){
                  var fName = entries[index].name;
                  var fDate = sixMonths[x].dateKey;
                  //console.log("date :: ", fDate);
                  if(fName.search(fDate) != -1){
                    knownFiles.push(entries[index]);
                  } //end if
                } //inner for
              } //outer for
              //console.log("knownFiles :: ",knownFiles);

              //entries.some(function(entry){

              filesLength = knownFiles.length;
              knownFiles.some(function(entry){
                var reqfile = entry.fullPath;
                d.getFile(reqfile,{},
                  function(fileEntry){
                    fileEntry.file(function(file){
                     // console.log('file ::', file);
                      var fName = file.name;
                      //console.log("filename :: ", fName);
                      var dateKey=fName.slice(0,-4).substr(-7);
                      var monthNumber=dateKey.substr(-2);
                      console.log("month# :: ", monthNumber);
                      var fullDate;
                      for(var x=0; x<sixMonths.length;x++){
                        if((sixMonths[x].dateKey).search(dateKey) != -1){
                          fullDate = sixMonths[x].fullDate;
                        }
                      }
                      Papa.parse(file,{
                        header: true,
                        dynamicTyping: true,
                        complete: function(){
                        //console.log(arguments);
                          var servicesUsedArr=[];
                          var parserObj = arguments[0];
                          var totAmt=0;
                          var servicesArr = []; //contains the various services used by an account

                          //storing the services used
                          for(var j=0;j<(parserObj.data.length)-1;j++){
                            var used = parserObj.data[j].ProductCode;
                            if(used){
                             // if(used != ""){
                                if(servicesUsedArr.indexOf(used) == -1) {
                                  servicesUsedArr.push(used);
                                }                           
                             // }
                            }
                          }  //end for

                          for(var j=0; j<servicesUsedArr.length; j++){ //each record in a file
                            var servicesUsed = _.where(parserObj.data,{ProductCode:servicesUsedArr[j]});
                          //  console.log('servicesUsed :: ', servicesUsed);
                            var totCost = 0;
                            for(var z=0;z<servicesUsed.length;z++){
                              if(servicesUsed[z].LinkedAccountId == ""){
                                totCost += servicesUsed[z].TotalCost ? parseInt(servicesUsed[z].TotalCost) : 0;
                              }
                            }
                            
                            var servicesObj = {'ProductCode': servicesUsed[0].ProductCode,
                                  'ProductName':servicesUsed[0].ProductName,
                                  'TotalCost' : totCost};
                            
                          //  console.log("service Obj :: ", servicesObj);

                            servicesArr.push(servicesObj);
                            totAmt += totCost;
                          } //end of for
                          
                          servicesArr.sort(function(a,b){
                            return b.totCost - a.totCost;
                          });

                          var payerObj={
                            'PayerAccountID' : parserObj.data[0].PayerAccountId,
                            'PayerAccountName' : parserObj.data[0].PayerAccountName,
                            'TotalAmount' : totAmt,
                            'fileDate' : monthNumber,
                            'DateKey' : fullDate,
                            'ProductsUsed' : servicesArr
                          };
                       //   console.log('payerObj',payerObj);
                          payerAccArr.push(payerObj);
                          //console.log('before adding all the data',payerAccArr.length+
                            //'::',dirLength);
                          
                          if(payerAccArr.length == filesLength){
                            //console.log('after adding all the data');
                            $('#loading1').removeClass('show').addClass('hide');
                            $('#content-Container').removeClass('hide').addClass('show');
                            payerAccSorted = payerAccArr.sort(function(a,b){
                               return b.fileDate - a.fileDate;
                              });
                            document.getElementById('admin').innerHTML="<img src='img/admin.png'> " + payerAccArr[0].PayerAccountName;
                            createViewPager();
                          }

                          var linkedObj
                        }, //end of complete
                        error: errorFn
                      }); //end Papa.parse
                    }); //end fileEntry.file
                  }); //end getFile
                }); //end some
             } //ends else
          }, onError);
        }, onError );
  }, onRequestError);
  //alert("Getting files");

}

function getSixMonthsData(){
  var newMonth;
  d = new Date();
  curMonth = (d.getMonth()) + 1;
  year = d.getFullYear();
  newMonth = curMonth;
  for(var i = 0; i<6;i++){
    thisMonth = newMonth<10 ? '0'+newMonth: newMonth;
    //lastFewMonths[i] = year +"-"+thisMonth;
    var dateKey = year +"-"+thisMonth;
    var dateObj = {'dateKey': dateKey,
            'fullDate':monthNames[newMonth-1] + '-' + year};
    newMonth = newMonth - 1;
    lastFewMonths.push(dateObj);
  }

  return lastFewMonths;
  //searchKey = year + '-'+month;

}