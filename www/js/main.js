//var items = item_container.children.length;

var offset = 0;

function createViewPager() {
  //console.log('create view pager');

  item_container = document.querySelector('.pager_items');
  //console.log('item container :: ', item_container);
  view_pager_elem = document.querySelector('.pager');
  w = view_pager_elem.getBoundingClientRect().width;
  items = payerAccSorted.length;
  item_container.style.width = (items * 100)+ '%';
  //console.log('item style width :: ', item_container.style.width);
  var child_width = (100 / items) + '%';
  var html = "";
  document.getElementById('monthInfo').innerHTML=payerAccSorted[0].DateKey + " Bill Amount ";
  for (var i = 0; i < items; i++) {
    html += "<div class=toggle><h4>Payer Account Name</h4> <ul> <li>" + payerAccArr[i].PayerAccountName + "</li> </ul> <h4>Amount</h4><ul> <li> " + payerAccArr[i].TotalAmount +"</li> </ul></div>";
  }
  item_container.innerHTML = html;
  
  for(var i=0;i<items;i++)
    item_container.children[i].style.width = child_width;

  showViewPager();
}

function showViewPager(){

  //createDoughnutChart();
  
  vp = new ViewPager(view_pager_elem, {
    pages: item_container.children.length,
    vertical: false,
    onPageScroll : function (scrollInfo) {
      console.log('onPageScroll', scrollInfo);
      offset = -scrollInfo.totalOffset;
      invalidateScroll();
    },

    onPageChange : function (page) {
      document.getElementById('monthInfo').innerHTML=payerAccArr[page].DateKey + " Bill Amount ";
      console.log('page', page);
      createDoughnutChart(page);
    }
  });
  
  window.addEventListener('resize', function () {
    w = view_pager_elem.getBoundingClientRect().width;
    invalidateScroll();
  });
  
  $('#canvas-holder').removeClass('hide').addClass('show');
  createDoughnutChart(index);
}

function invalidateScroll() {
  item_container.style['-webkit-transform'] = 'translate3d(' + (offset * w) + 'px, 0px, 0px)';
}

document.getElementById('btn-prev').addEventListener('click', function () {
  vp.previous();
  if(index>0){
    createDoughnutChart(index--);
  }
});

document.getElementById('btn-next').addEventListener('click', function () {
  vp.next();
  if(index<filesLength){
    createDoughnutChart(++index);
  }
});

function createDoughnutChart(index){
  var indexer = 0;

  var productsLength =  payerAccArr[index].ProductsUsed.length;
  if (productsLength > 5){
    productsLength = 5
  }
    
  var colorArray = ["#F7464A","#46BFBD","#FDB45C","#949FB1","#4D5360"];
  var highlightArr=["#FF5A5E","#5AD3D1","#FFC870","#A8B3C5","#616774"];
  var doughnutData=[];
  for(indexer=0;indexer<productsLength;indexer++){
    var dataObj = {
      value: payerAccArr[index].ProductsUsed[indexer].TotalCost,
      color:colorArray[indexer],
      highlight: highlightArr[indexer],
      label: payerAccArr[index].ProductsUsed[indexer].ProductCode
    };

    doughnutData.push(dataObj);
  }

     // window.onload = function(){
        var ctx = document.getElementById("chart-area").getContext("2d");
        ctx.canvas.width = 268;
        ctx.canvas.height = 259;
        window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {maintainAspectRatio: false});
     // };

      $('#fileList').removeClass('hide').addClass('show');
      showServicesList(index);
    // ++indexer;
}

function showServicesList(index){
  indexer=0;
  var productsLength =  payerAccSorted[index].ProductsUsed.length;
  if (productsLength > 5){
    productsLength = 5
  }
  else{
    productsLength =  payerAccSorted[index].ProductsUsed.length;
  }

  var html="";
  for(indexer=0;indexer<productsLength;indexer++){
    html += '<li><p class="text-cls"><bold>' + payerAccSorted[index].ProductsUsed[indexer].ProductCode + '</bold></p><p class="value-cls"> $' + 
        payerAccArr[index].ProductsUsed[indexer].TotalCost +'</span></li>'; 
  }

  document.getElementById('list').innerHTML = html;

  populateServicesList(index);
}

function populateServicesList(index){
  var productsLength = payerAccArr[index].ProductsUsed.length;

  var html="";
  
  for(indexer=0;indexer<productsLength;indexer++){
    html += '<li><p class="text-class" ><bold>' + payerAccArr[index].ProductsUsed[indexer].ProductCode + '</bold></p><p class="value-class"> $' + payerAccArr[index].ProductsUsed[indexer].TotalCost +'<br /><br /></p><span>' +
            payerAccArr[index].ProductsUsed[indexer].ProductName + '</span></li>'; 
  }

  document.getElementById('list1').innerHTML=html;
}